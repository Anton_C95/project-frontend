import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-cars-admin',
  templateUrl: './cars-admin.component.html',
  styleUrls: ['../common.css'],
  providers: [BackendService]
})
export class CarsAdminComponent implements OnInit {
  cars: any;
  areas: any;

  constructor(private backend: BackendService) { }

  // Both cars data and area data is fetched onInit, the areas are required for 
  // displaying possible areas for relocating the car
  ngOnInit(): void {
    this.backend.getCars().subscribe(cars => {
      this.cars = cars;
    });  
    this.backend.getAreas().subscribe(areas => {
      this.areas = areas;
    });
  }

  // Calls the backedn deleteCar function which removes given car from the cars collection
  deleteCar(id: String) {
    this.backend.deleteCar(id).subscribe(() => {
      window.location.reload(); // Reload the page to fetch the updated data
    });
  }

  // Creates a form from the values in the options tag, and passes it to the updateCarArea (PUT) function
  // which updates the area of the car, this can only be done if no customers are in the area.
  // Cars that are in the area to be deleted are redeployed to the area with least number of vehicles.
  updateCarArea(area: String, car: String) {
    const form = {
      Registration_number: car,
      Area: area,
    }
    this.backend.updateCarArea(form).subscribe(() => {
      window.location.reload(); // Reload the page to fetch the updated data  
    });
  }

  
}
