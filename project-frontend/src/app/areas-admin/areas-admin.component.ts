import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-areas-admin',
  templateUrl: './areas-admin.component.html',
  styleUrls: ['../common.css'],
  providers: [BackendService]
})
export class AreasAdminComponent implements OnInit {
  
  areas: any; 
  constructor(private backend: BackendService) { }

  // onInit the data for the page is fetched to become part of the model
  ngOnInit(): void {
    this.backend.getAreas().subscribe(areas => {
      this.areas = areas;
    });
  }

  // Calls the deleteArea function which sends the area to the server which in turn deletes
  // the document in the areas collection.
  deleteArea(area: String) {
    this.backend.deleteArea(area).subscribe(() => {
      window.location.reload();  // runs the ngOnInit function again to reload the data.
    });
  }
}
