import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-car-add-form',
  templateUrl: './car-add-form.component.html',
  styleUrls: ['../common.css'],
  providers: [BackendService]
})
export class CarAddFormComponent implements OnInit {

  areas: any;
  years: any = <any>[];  

  constructor(private backend: BackendService) { }

  // On page load the areas are fetched so that they can be displayed in the select
  // tag for the user choose an area to deploy the new car.
  ngOnInit(): void {
    this.backend.getAreas().subscribe(areas => {
      this.areas = areas;
    });
    // Create an array of years for the user to choose production year of the new car.
    for(let i = 2000; i < 2022; i++) {  // Create an array of years for the select tag
      this.years.push(i);
    }
  }

  // Calls the backend function addCar together with the form data.
  addCar(form: any) {
    let rates = [form.rateMember, form.rateNonMember]  // As two data points are required in this field a new array is declared
    // with the inputted values assigned to the array, key value pairs are then deleted as they are not required any further.
    delete form.rateMember;
    delete form.RateNonMember;
    form.rate = rates;
    this.backend.addCar(form).subscribe(() => {
      window.location.reload();
    });
  }
}
