import { Component, OnInit } from '@angular/core';
import { BackendService} from '../backend.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['../common.css'],
  providers: [ BackendService]
})
export class CarDetailComponent implements OnInit {

  dataLoaded = false;
  car = <any>{}
  constructor(private backend: BackendService, private route: ActivatedRoute) {}

  // Get the details of the car
  ngOnInit(): void { 
    this.backend.getCarDetails(this.route.snapshot.params['id']).subscribe((result) => {
      this.car = result[0];
      this.dataLoaded = true;  // Added ngIf statement for the section to show when the data has been loaded, resolvers the type error undefined issue.
    })
  }

}
