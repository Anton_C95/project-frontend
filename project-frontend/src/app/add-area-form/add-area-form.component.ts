import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-add-area-form',
  templateUrl: './add-area-form.component.html',
  styleUrls: ['../common.css'],
  providers: [BackendService]
})
export class AddAreaFormComponent implements OnInit {

  constructor(private backend: BackendService) { }

  ngOnInit(): void {}

  // Runs the backend function addArea which sends the data collected in the form
  // to be added in the areas collection.
  addArea(form: any) {
    this.backend.addArea(form).subscribe(() => {
      window.location.reload();
    });
  }
}
