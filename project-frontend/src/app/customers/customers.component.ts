import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['../common.css'],
  providers: [BackendService]
})
export class CustomersComponent implements OnInit {

  customers: any; 

  constructor(private backend: BackendService) { }

  // Fethes the data from the customers collection, all data points are displayed in this page.
  ngOnInit(): void {
    this.backend.getCustomers().subscribe(customers => {
      this.customers = customers;
    });
  }

}
