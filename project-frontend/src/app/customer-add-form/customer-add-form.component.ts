import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';


@Component({
  selector: 'app-customer-add-form',
  templateUrl: './customer-add-form.component.html',
  styleUrls: ['../common.css'],
  providers: [BackendService]
})
export class CustomerAddFormComponent implements OnInit {

  areas: any;
  constructor(private backend: BackendService) { }

  // The areas are displayed in the select tag for the user to choose from when assigning the new customer an area
  ngOnInit(): void {
    this.backend.getAreas().subscribe(areas => {
      this.areas = areas;
    });
  }

  // Calls the addCustomer backend function which adds the customer information in the form to the customers collection.
  addCustomer(form: any) {
    form.car = "";  // All customers are added with an empty car field. 
    console.log(form);
    this.backend.addCustomer(form).subscribe(() => {
      window.location.reload();
    })
  }
}
