import { Component, OnInit } from '@angular/core';
import { BackendService } from '../backend.service';

@Component({
  selector: 'app-customers-admin',
  templateUrl: './customers-admin.component.html',
  styleUrls: ['../common.css'],
  providers: [BackendService]
})
export class CustomersAdminComponent implements OnInit {

  customers: any;
  cars: any;
  areas: any;
  value: any;
  myselect: any;

  constructor(private backend: BackendService) { }

  // Apart from the customers data hte data for cars and areas are fetched to display available cars and areas for the customer
  // to change to. 
  ngOnInit(): void {
    this.cars = <any>[];
    this.backend.getCustomers().subscribe(customers => {
      this.customers = customers;
    });  
    this.backend.getCars().subscribe(cars => {
      for (let i = 0; i < cars.length; i++) {
        if (cars[i].Driver === undefined) {  // Only those cars without drivers are to be part of the model.
          this.cars.push(cars[i])
        }
      }
    });
    this.backend.getAreas().subscribe(areas => {
      this.areas = areas;
    });
  }

  // Delete customer with given id
  delete(id: number) {
    this.backend.deleteCustomer(id).subscribe(() => {
      window.location.reload();  // Reload the page to fetch the updated data
    });
  }

  // Release the car currently rented by the customer
  dropCar(id: number) {
    this.backend.dropCar(id).subscribe(() => {
      window.location.reload();  // Reload the page to fetch the updated data
    });
  }

  // Change the membership status of the customer
  updateMembership(id: number) {
    this.backend.updateMembership(id).subscribe(() => {  
      window.location.reload();  // Reload the page to fetch the updated data  
    });
  }
 
  // If a customer is not currently renting a car, the customer is assigned the car given.
  updateCar(value: any, id: any) { 
    const form = {
      id: id,
      Car: value
    }
    this.backend.updateCar(form).subscribe(() => {
      window.location.reload();  // Reload the page to fetch the updated data
    });
  }

  // Updates the area of the customer, if the customer is currently renting a car, the cars' area
  // will also be updated
  updateCustomerArea(value: any, id: any) {
    const form = {
      id: id,
      Area: value
    }
    console.log(value);
    console.log(id)
    this.backend.updateCustomerArea(form).subscribe((result) => {
      console.log(result);
      window.location.reload();  // Reload the page to fetch the updated data
    });
  }
}
