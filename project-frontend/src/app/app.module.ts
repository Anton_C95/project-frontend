import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CustomersComponent } from './customers/customers.component';
import { HttpClientModule } from '@angular/common/http';
import { CustomersAdminComponent } from './customers-admin/customers-admin.component';
import { AreasAdminComponent } from './areas-admin/areas-admin.component';
import { CarsAdminComponent } from './cars-admin/cars-admin.component';
import { CustomerAddFormComponent } from './customer-add-form/customer-add-form.component';
import { FormsModule } from '@angular/forms';
import { CarAddFormComponent } from './car-add-form/car-add-form.component';
import { AddAreaFormComponent } from './add-area-form/add-area-form.component';
import { CarDetailComponent } from './car-detail/car-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomersComponent,
    CustomersAdminComponent,
    AreasAdminComponent,
    CarsAdminComponent,
    CustomerAddFormComponent,
    CarAddFormComponent,
    AddAreaFormComponent,
    CarDetailComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
