import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BackendService {

  url = "https://carshareapp.herokuapp.com/api/";

  constructor(private http: HttpClient) { }

  // Fetches all the customer information
  getCustomers(): Observable<any> {
    return this.http.get(this.url + "customers")
  }

  // Fetch the data from the areas collection an the number of customers and cars aggregated from 
  // the cars and cusotmers collection.
  getAreas(): Observable<any> {
    return this.http.get(this.url + "areas")
  }

  // Fetch cars and in the case that a car is currently being rented return the aggregated id and name of the customer.
  getCars(): Observable<any> {
    return this.http.get(this.url + "cars");
  }

  getCarDetails(id: String):  Observable<any> {
    return this.http.get(this.url + "cars/" + id);
  }

  // Update area for customer and car
  updateCustomerArea(form: any): Observable<any> {
    return this.http.put(this.url + "customers/update-area", form);
  }

  // Deletes the customer if not currently renting
  deleteCustomer(id: number): Observable<any> {
    return this.http.delete(this.url + "customers/delete/" + id);
  }

  addCustomer(form: any): Observable<any> {
    return this.http.post(this.url + "customers/add", form);
  }

  dropCar(id: number): Observable<any> {
    return this.http.put(this.url + "customers/drop-car/" + id, {});
  }

  updateMembership(id: number): Observable<any> {
    return this.http.put(this.url + "customers/update-membership/" + id, {});
  }

  // If the user is not currently renting the car is assigned to the customer
  updateCar(form: any): Observable<any> {
    return this.http.put(this.url + "customers/rent-car", form);
  }

  addCar(form: any): Observable<any> {
    return this.http.post(this.url + "cars/add", form)
  }

  // Delete car if not currently rented
  deleteCar(id: String): Observable<any> {
    return this.http.delete(this.url + "cars/delete/" + id);
  }

  // Redeploys the car if it is not currently being rented
  updateCarArea(form: any): Observable<any> {
    return this.http.put(this.url + "cars/update/area", form);
  }

  addArea(form: any): Observable<any> {
    return this.http.post(this.url + "areas/add", form);
  }

  deleteArea(area: String): Observable<any> {
    return this.http.delete(this.url + "areas/delete/" + area);
  }
}
