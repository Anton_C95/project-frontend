import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreasAdminComponent } from './areas-admin/areas-admin.component';
import { CarDetailComponent } from './car-detail/car-detail.component';
import { CarsAdminComponent } from './cars-admin/cars-admin.component';
import { CustomersAdminComponent } from './customers-admin/customers-admin.component';
import { CustomersComponent } from './customers/customers.component';

const routes: Routes = [{
  path: '',
  component: CustomersComponent
},{
  path: 'customers',
  component: CustomersComponent
}, {
  path: 'customers-admin',
  component: CustomersAdminComponent
},
{
  path: 'areas-admin',
  component: AreasAdminComponent
},
{
  path: 'cars-admin',
  component: CarsAdminComponent
},
{
  path: 'cars/:id',
  component: CarDetailComponent
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
